git config --global user.name "Muhammad Zaki Faizal"
git config --global user.email "mzaki194@gmail.com"

Create a new repository
git clone git@gitlab.com:mzakif/garam2.git
cd garam2
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:mzakif/garam2.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:mzakif/garam2.git
git push -u origin --all
git push -u origin --tags